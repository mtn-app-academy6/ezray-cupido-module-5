import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/Login.dart';
import 'package:flutter_application_1/UserProfileEdit.dart';
import 'package:lottie/lottie.dart';
import 'package:firebase_core/firebase_core.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: FirebaseOptions(
          apiKey: "AIzaSyA9265XE9byfVG8ApZciIMN9aGa2WWaAnY",
          authDomain: "mtnapp-39e16.firebaseapp.com",
          projectId: "mtnapp-39e16",
          storageBucket: "mtnapp-39e16.appspot.com",
          messagingSenderId: "162166970397",
          appId: "1:162166970397:web:b7e95b0118ac725d374ef4"));
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute:'/',
        routes: {
          '/': (context) => SplashScreen(),
          '/screen2': (context) => Registration(),
          '/screen3': (context) => Dashboard(),
          '/screen4': (context) => FeatureScreen1(),
          '/screen5': (context) => FeatureScreen2(),
          '/screen6': (context) => UserProfileEdit(),
        },
        theme: ThemeData.dark());
  }
}

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
        splash: Lottie.asset('assets/98955-hexagon-loading.json'),
        nextScreen: const Login(),
        splashIconSize: 200,
        duration: 8500,
        splashTransition: SplashTransition.fadeTransition,
        backgroundColor: Colors.tealAccent);
  }
}
