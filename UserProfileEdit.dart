import 'dart:developer';
import 'dart:html';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/sessionList.dart';

class UserProfileEdit extends StatelessWidget {
  const UserProfileEdit({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("UserProfileEdit"),
        ),
        body: Column(children: [
          Center(
              child: SizedBox(
                  height: 115,
                  width: 115,
                  child: Stack(
                      fit: StackFit.expand,
                      clipBehavior: Clip.none,
                      children: [
                        CircleAvatar(
                            backgroundImage: AssetImage('assets/Avatar.jpg'))
                      ]))),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(
              height: 20,
              width: 115,
              child: Stack(),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: const TextField(
                obscureText: true,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Name',
                )),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(
              height: 20,
              width: 115,
              child: Stack(),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: const TextField(
                obscureText: true,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Surname',
                )),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: SizedBox(
              height: 20,
              width: 115,
              child: Stack(),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: const TextField(
                obscureText: true,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Age',
                )),
          ),
          ElevatedButton(onPressed: () {}, child: Text('Register'))
        ]));
  }
}

class FeatureScreen1 extends StatefulWidget {
  const FeatureScreen1({Key? key}) : super(key: key);

  @override
  State<FeatureScreen1> createState() => _FeatureScreen1State();
}

class _FeatureScreen1State extends State<FeatureScreen1> {
  @override
  Widget build(BuildContext context) {
    TextEditingController subjectController = TextEditingController();
    TextEditingController locationController = TextEditingController();
    TextEditingController emailController = TextEditingController();

    Future _addSessions() {
      final subject = subjectController.text;
      final location = locationController.text;
      final email = emailController.text;

      final ref = FirebaseFirestore.instance.collection('sessions').doc();

      return ref
          .set(
              {'Subject Name': subject, 'location': location, 'email': email,"doc_id": ref.id,})
          .then((value) =>
              {subjectController.text = "", locationController.text = "", emailController.text = ""})
          .catchError((onError) => log(onError));
    }

    return Scaffold(
        appBar: AppBar(title: const Text('FeatureScreen1')),
        body: SingleChildScrollView(
            child: Column(
          children: [
            Column(children: [
              SizedBox(
                height: 115,
                width: 115,
                child: Stack(),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                    controller: subjectController,
                    obscureText: true,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Name',
                    )),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                    controller: locationController,
                    obscureText: true,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Surname',
                    )),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: TextField(
                    controller: emailController,
                    obscureText: true,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Email',
                    )),
              ),
              ElevatedButton(
                  onPressed: () {
                    _addSessions();
                  },
                  child: Text('Save'))
            ]),
            SessionList()
          ],
        )));
  }
}

class FeatureScreen2 extends StatelessWidget {
  const FeatureScreen2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Feature 2"),
        ),
        body: ElevatedButton(
          onPressed: () => {
            Navigator.pushNamed(context, '/screen6'),
          },
          child: const Text("Go to Profile"),
        ));
  }
}
