import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

class SessionList extends StatefulWidget {
  const SessionList({Key? key}) : super(key: key);

  @override
  State<SessionList> createState() => _SessionListState();
}

class _SessionListState extends State<SessionList> {

  final Stream<QuerySnapshot> _mySessions =
      FirebaseFirestore.instance.collection("sessions").snapshots();

  @override
  Widget build(BuildContext context) {
    TextEditingController _subjectFieldCtrlr = TextEditingController();
    TextEditingController _locationFieldCtrlr = TextEditingController();
    TextEditingController _emailFieldCtrlr = TextEditingController();

    void _delete(docId) {
      FirebaseFirestore.instance
          .collection("sessions")
          .doc(docId)
          .delete()
          .then((value) => print("deleted"));
    }

    void _update(data) {
      var collection = FirebaseFirestore.instance.collection("sessions");
      _subjectFieldCtrlr.text = data["Subject Name"];
      _locationFieldCtrlr.text = data["location"];
      _emailFieldCtrlr.text = data["email"];

      showDialog(
          context: context,
          builder: (_) => AlertDialog(
                title: Text("Edit"),
                content: Column(mainAxisSize: MainAxisSize.min, children: [
                  TextField(
                    controller: _subjectFieldCtrlr,
                  ),
                  TextField(
                    controller: _locationFieldCtrlr,
                  ),
                  TextField(
                    controller: _emailFieldCtrlr,
                  ),
                  TextButton(
                      onPressed: () {
                        collection.doc(data["doc_id"]).update({
                          "Subject Name": _subjectFieldCtrlr.text,
                          "location": _locationFieldCtrlr.text,
                          "email": _emailFieldCtrlr.text,
                        });
                        Navigator.pop(context);
                      },
                      child: Text("Update"))
                ]),
              ));
    }

    return StreamBuilder(
      stream: _mySessions,
      builder: (BuildContext context,
          AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if (snapshot.hasError) {
          return Text("something went wrong");
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        }
        if (snapshot.hasData) {
          return Row(
            children: [
              Expanded(
                child: SizedBox(
                    height: (MediaQuery.of(context).size.height),
                    width: MediaQuery.of(context).size.width,
                    child: ListView(
                      children: snapshot.data!.docs
                          .map((DocumentSnapshot documentSnapshot) {
                        Map<String, dynamic> data =
                            documentSnapshot.data()! as Map<String, dynamic>;
                        return Column(
                          children: [
                            Card(
                              child: Column(children: [
                                ListTile(
                                  title: Text(data['Subject Name']),
                              
                                  subtitle: Text(data['location']),
                                  isThreeLine: true,
                                  trailing:Text(data['email']),
                                
                                ),
                                ButtonTheme(
                                    child: ButtonBar(
                                  children: [
                                    OutlinedButton.icon(
                                      onPressed: () {
                                        _update(data);
                                      },
                                      icon: Icon(Icons.edit),
                                      label: Text("Edit"),
                                    ),
                                    OutlinedButton.icon(
                                      onPressed: () {
                                        _delete(data["doc_id"]);
                                      },
                                      icon: Icon(Icons.remove),
                                      label: Text("Delete"),
                                    )
                                  ],
                                ))
                              ]),
                            )
                          ],
                        );
                      }).toList(),
                    )),
              )
            ],
          );
        } else {
          return (Text("No Data"));
        }
      },
    );
  }
}
